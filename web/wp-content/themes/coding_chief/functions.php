<?php

require_once( 'core/core.php' );
require_once( 'core/competition_modal.php' );
require_once( 'core/competition_winner_modal.php' );
require_once( 'core/load_more.php' );
require_once( 'core/load_more_news.php' );
require_once( 'core/update_fav_count.php' );
require_once( 'core/form_reload.php' );

/*
add_filter( 'gform_validation_message', 'sw_gf_validation_message', 10, 2 );
function sw_gf_validation_message( $validation_message ) {
    add_action( 'wp_footer', 'sw_gf_js_error' );
}
function sw_gf_js_error() { ?>
    <script type="text/javascript">
        alert( "Error: Please fill up the form properly." );
    </script>
<?php }
*/