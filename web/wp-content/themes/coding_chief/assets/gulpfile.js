var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var filesExist = require('files-exist');

var AUTOPREFIXER_BROWSERS = [
      'ie >= 9',
      'ie_mob >= 10',
      'ff >= 30',
      'chrome >= 34',
      'safari >= 7',
      'opera >= 23',
      'ios >= 7',
      'android >= 4.4',
      'bb >= 10'
    ];

var paths = {
  style: {
    dest: './css/',
    vendor: [
      './vendor/vendor.scss'
    ],
    app: [
      './scss/app.scss'
    ],
  editor: [
      './scss/editor-style.scss'
    ]
  },
  js: {
    dest: './js/',
    vendor: [
      // './js/popper.min.js', // stupid gulp has issues with popper's node_modules folder containing a dot (popper.js/)
      './node_modules/bootstrap/dist/js/bootstrap.min.js',
      './node_modules/bootstrap-select/js/bootstrap-select.js',
      './node_modules/slick-carousel/slick/slick.min.js'
      //'node_modules/skrollr/dist/skrollr.min.js',
    ],
    app: [
      './js/app-src.js'
    ]
  }
};

/**
 * Style
 */

gulp.task('style.app', function() {
    gulp.src(paths.style.app)
        .pipe($.plumber())
        .pipe($.newer(paths.style.dest))
        .pipe($.sourcemaps.init())
          .pipe($.sass({
            includePaths: ['stylesheets', './node_modules/bootstrap/scss'],
            precision: 10
          }))
          .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
          .pipe($.concat('app.min.css'))
          .pipe($.if('*.css', $.cssnano({zindex: false})))
          .pipe($.size({title: 'stylesheets'}))
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(paths.style.dest))
        .pipe($.livereload());
});

gulp.task('style.editor', function() {
    gulp.src(paths.style.editor)
        .pipe($.plumber())
        .pipe($.newer(paths.style.dest))
        .pipe($.sourcemaps.init())
          .pipe($.sass({
            includePaths: ['stylesheets', './node_modules/bootstrap/scss'],
            precision: 10
          }))
          .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
          .pipe($.concat('editor-style.min.css'))
          .pipe($.if('*.css', $.cssnano({zindex: false})))
          .pipe($.size({title: 'stylesheets'}))
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(paths.style.dest))
        .pipe($.livereload());
});

gulp.task('style.vendor', function() {
    gulp.src(paths.style.vendor)
        .pipe($.plumber())
        .pipe($.newer(paths.style.dest))
        .pipe($.sourcemaps.init())
          .pipe($.sass({
            includePaths: ['stylesheets', './node_modules/bootstrap/scss'],
            precision: 10
          }))
          .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
          .pipe($.concat('vendor.min.css'))
          .pipe($.if('*.css', $.cssnano({zindex: false})))
          .pipe($.size({title: 'stylesheets'}))
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(paths.style.dest))
        .pipe($.livereload());
});

/**
 * JavaScript
 */

gulp.task('js.app', function() {
    return gulp.src(paths.js.app)
        .pipe($.plumber())
        // .pipe($.newer(paths.js.dest))
        .pipe($.size({title: 'js.app before'}))
        // .pipe($.sourcemaps.init())
        .pipe($.concat('app.js'))
        .pipe($.minify({
            ext:{
                src:'-debug.js',
                min:'.js'
            },
            ignoreFiles: ['-min.js']
        }))
        // .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(paths.js.dest))
        .pipe($.size({title: 'js.app after'}))
        .pipe($.livereload());
});

gulp.task('js.vendor', function() {
    return gulp.src(filesExist(paths.js.vendor), {dot: true})
        .pipe($.plumber())
        // .pipe($.newer(paths.js.dest))
        .pipe($.size({title: 'js.vendor before'}))
        // .pipe($.sourcemaps.init())
        .pipe($.concat('vendor.js'))
        .pipe($.minify({
            ext:{
                src:'-debug.js',
                min:'.js'
            },
            ignoreFiles: ['-min.js']
        }))
        // .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(paths.js.dest))
        .pipe($.size({title: 'js.vendor after'}))
        .pipe($.livereload());
});

/**
 * Global Tasks
 */

gulp.task('js', ['js.app', 'js.vendor']);
gulp.task('app', ['style.app', 'style.editor', 'style.vendor', 'js']);

gulp.task('watch', function() {
    $.livereload.listen();
    gulp.watch('./scss/**/*.scss', ['style.app', 'style.editor']);
    gulp.watch(paths.js.app, ['js.app']);
  gulp.watch(paths.js.vendor, ['js.vendor']);
});

gulp.task('dist', ['app']);
gulp.task('dev', ['app', 'watch']);

gulp.task('default', ['dev']);
