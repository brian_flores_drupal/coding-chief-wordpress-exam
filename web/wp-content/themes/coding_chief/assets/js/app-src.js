var App = ( function( document, window, $ ) {

	function App() {
		this.$slick = $('[data-slick]');
		this.$body = $('body');
	}

	App.prototype.init = function() {
		this.enableSlick();
		this.formUI();
		this.headerMobile();
		// this.affix();
		this.enableHeader();
		this.smoothScroll();
		this.modalEnable();
		// this.toggleAffix();
	};
	App.prototype.headerMobile = function() {
		var self = this;
		$('.menu-toggle').on('click', function(){
			self.$body.removeClass('user-login-open').toggleClass('mobile-menu-open');
		});
		$('.user-login-toggle').on('click', function(){
			self.$body.removeClass('mobile-menu-open').toggleClass('user-login-open');
		});
	};

	
	/**
	 * example
	 */
	App.prototype.enableSlick = function() {
		var slickPrimarySecondary = {
	       slidesToShow: 1,
	       slidesToScroll: 1,
	       arrows: false,
	       infinite: true,
	       asNavFor: '.slider-for',
	       focusOnSelect:true,
	       draggable: false,
	       centerMode: false,
	     };
	     var slickNavigator = {
	       slidesToShow: 1,
	       slidesToScroll: 1,
	       asNavFor: '.slider-nav',
	       speed: 500,
	       dots: true,
	       arrows: false,
	       draggable: false,
	       focusOnSelect:true,
	       centerMode: false,
	       infinite: true
	     };
	     var defaults = {
	       slidesToShow: 1,
	       slidesToScroll: 1,
	       speed: 500,
	       dots: true,
	       arrows: false,
	       draggable: false,
	       focusOnSelect:true,
	       centerMode: false,
	       infinite: true
	     };
	     $('.slide-images').slick(slickPrimarySecondary);
	     
	     $('.slide-featured-contents').slick(slickNavigator);

	     $('.slider-latest-news').slick(defaults);
	};
	App.prototype.formUI = function() {
		$('.bootstrap-select-enable select.gfield_select').each(function(){
			$(this).selectpicker();
		});
	};
	App.prototype.enableHeader = function() {
		$('header')
			.on('click', '[href="#mobileMenuToggle"]', function(e){
				e.preventDefault();
				$('html').toggleClass('mobile-menu-open');
			})
			.on('click', '.mobile-expander', function(){
				$(this).parent('li').toggleClass('expanded');
			});
	};
	App.prototype.modalEnable = function() {
		$('.youtube.thumbnail').on('click', function(){
			$(this).siblings('.video-item').clone().show().appendTo('.modal-video .video-item-wrapper ');
			$('.modal-video').addClass('active');
		});
		$('.modal-close').on('click', function(){
			$(this).parents('.modal-video').removeClass('active').find('.video-item-wrapper').empty();
		});
	};
	return App;

} )( document, window, jQuery );

var app = new App();
app.init();


/*==================================================
 *	ACF Map front end
 ================================================*/

(function($) {

	function new_map( $el ) {
		var $markers = $el.find('.marker'),
			args = {
				zoom: 17,
				center: new google.maps.LatLng(0, 0),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				disableDefaultUI: true
			},
			map = new google.maps.Map( $el[0], args );

		map.markers = [];
		$markers.each(function(){
			add_marker( $(this), map );
		});
		center_map( map );
		return map;
	}

	function add_marker( $marker, map ) {
		var image = {
	        url:  "/wp-content/themes/dhs/assets/images/icon-location.svg", // url
	        scaledSize: new google.maps.Size(24, 36), // size
	    };
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') ),
			marker = new google.maps.Marker({ position: latlng, map: map, icon: image });

		map.markers.push( marker );

		if( $marker.html() ) {
			var infowindow = new google.maps.InfoWindow({
				content: $marker.html()
			});

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open( map, marker );
			});
		}
	}

	function center_map( map ) {
		var bounds = new google.maps.LatLngBounds(),
			latlng;
		$.each( map.markers, function( i, marker ){
			latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
			bounds.extend( latlng );
		});

		if ( map.markers.length === 1 ) {
			map.setCenter( bounds.getCenter() );
			map.setZoom( 17 );
		} else {
			map.fitBounds( bounds );
		}
	}

	var map = null;

	$(document).ready(function(){
		$('.acf-map').each(function(){
			map = new_map( $(this) );
		});
		$('.slick-outer').on('setPosition', function () {
			$(this).find('.slick-slide').height('auto');
			var slickTrack = $(this).find('.slick-track');
			var slickTrackHeight = $(slickTrack).height();
			$(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
		});
	});

})(jQuery);
